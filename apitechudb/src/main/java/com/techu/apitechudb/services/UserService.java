package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.sound.midi.Soundbank;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> getUsers() {
        System.out.println("getUsers en UserService");

        return this.userRepository.findAll();
    }

    public List<UserModel> getUsersOrderByAge() {
        System.out.println("getUsersOrderByAge en UserService");
        System.out.println("Ordenando por edad");

        return this.userRepository.findAll(Sort.by(Sort.Direction.ASC, "age"));

    }

    public List<UserModel> getUsers(String orderByAge) {
        System.out.println("getUsers en UserService");

        List<UserModel> result;

        if(orderByAge != null){
        System.out.println("Se ha pedido ordenación");

        result = this.userRepository.findAll(Sort.by(("age")));
        } else {
            result = this.userRepository.findAll();
        }
        return result;
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("findById en UserService");

        return this.userRepository.findById(id);
    }

    public UserModel addUser(UserModel user) {
        System.out.println("addUser en UserService");

        return this.userRepository.save(user);
    }

    public UserModel update(UserModel user) {
        System.out.println("update en UserService");

        return this.userRepository.save(user);
    }

    public boolean delete(String id) {
        System.out.println("delete en UserService");
        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("Usuario encontrado, borrando");
            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }


}
