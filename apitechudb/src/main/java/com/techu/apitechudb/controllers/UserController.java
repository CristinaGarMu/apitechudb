package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;


//Mi version ordenada
    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam(value = "orderByAge", defaultValue = "NO") String orderByAge) {
        System.out.println("getUsers");

        if(orderByAge.equals("YES")) {
            System.out.println("orderByAge = " + orderByAge);
            return new ResponseEntity<>(this.userService.getUsersOrderByAge(), HttpStatus.OK);
        }

        System.out.println("orderByAge = " + orderByAge);
        System.out.println("Devolviendo resultado sin orden por edad");
        return new ResponseEntity<>(this.userService.getUsers(), HttpStatus.OK);
    }

    //La version ordenada del profesor
//    @GetMapping("/users")
//    public ResponseEntity<List<UserModel>> getUsers(
//            @RequestParam(value = "$orderby", required = false) String orderByAge) {
//        System.out.println("getUsers");

//        return new ResponseEntity<>(this.userService.getUsers(orderByAge), HttpStatus.OK);
//    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);

        // operador ternario -> condicion ? Resultado si TRUE : Resultado si FALSE
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("La id del usuario a crear es " + user.getId());
        System.out.println("El nombre del usuario a crear es " + user.getName());
        System.out.println("La edad del usuario a crear es " + user.getAge());

        return new ResponseEntity<>(this.userService.addUser(user),  HttpStatus.CREATED);

    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser");
        System.out.println("La id del usuario a actualizar en parámetro URL es " + id);
        System.out.println("La id del usuario es " + user.getId());
        System.out.println("El nombre del usuario a actualizar es " + user.getName());
        System.out.println("La edad del usuario a actualizar es " + user.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById(id);
        if(userToUpdate.isPresent()) {
            System.out.println("Usuario para actualizar encontrado, actualizando");
            this.userService.update(user);
        }

        return new ResponseEntity<>(user,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("La id del usuario a borrar es " + id);

        boolean deletedUser = this. userService.delete(id);
        return new ResponseEntity<>(
                deletedUser ? "Usuario borrado" : "Usuario no borrado",
                deletedUser ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}
